package project.main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import project.parser.Parser;

public class Main extends JPanel implements ActionListener{

	private static final long serialVersionUID = 8731231735548721030L;
	
	JButton go;   
	JFileChooser chooser;
	String choosertitle;
   
	public Main() {
		go = new JButton("Select Directory");
		go.addActionListener(this);
		add(go);
	}

	public void actionPerformed(ActionEvent e) {
		String result;
        
		chooser = new JFileChooser(); 
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle(choosertitle);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		// disable the "All files" option.
		chooser.setAcceptAllFileFilterUsed(false); 

		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
			result = chooser.getCurrentDirectory().toString();
			System.out.println(result);
			
			try {
				Parser.parseCodebase(result);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		else {
			System.out.println("No Selection ");
		}
	}
   
	public Dimension getPreferredSize(){
		return new Dimension(200, 200);
    }
    
	public static void main(String s[]) {		
		Main panel = new Main();
		panel.setSize(100, 100);
		
		JFrame frame = new JFrame("Code Visualizer");
		frame.setSize(200, 200);
		frame.add(panel);
		frame.setVisible(true);
		frame.addWindowListener(
			new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					System.exit(0);
				}
			}
		);
	}

}
